package com.soliciti.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="users")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id")
	private Long id; 
	
	@NotNull
	@Column(name="user_email", unique=true)
	private String email; 
		
	@NotNull
	@Column(name="user_password")
	private String password; 
	
	@NotNull
	@Column(name="user_url", unique=true)
	private String url; 
	
	@Column(name="user_verified")
	private int verified; 
	
	@Column(name="user_type")
	private String userType;
	
	@SuppressWarnings("unused")
	private Long getId(){
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getVerified() {
		return verified;
	}

	public void setVerified(int verified) {
		this.verified = verified;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	private Account() {
	}
	
	
}
