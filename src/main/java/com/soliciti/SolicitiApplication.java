package com.soliciti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolicitiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolicitiApplication.class, args);
	}

}
