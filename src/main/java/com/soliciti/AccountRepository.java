package com.soliciti;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.soliciti.domain.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long>{

}
