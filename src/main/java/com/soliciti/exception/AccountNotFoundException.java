package com.soliciti.exception;

public class AccountNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1501512804905134787L;

	public AccountNotFoundException(String msg){
		super(msg);
	}
}
