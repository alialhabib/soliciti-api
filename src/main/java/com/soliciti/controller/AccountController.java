package com.soliciti.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.soliciti.domain.Account;
import com.soliciti.exception.AccountNotFoundException;
import com.soliciti.service.AccountService;

@RestController
@RequestMapping("/api/1.0/accounts")
@Secured("ROLE_ADMIN")
public class AccountController {
	
	private AccountService accountService; //all business logic is here
	
	@Autowired
	public AccountController (AccountService accountService){
		this.accountService = accountService;
	}
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public Iterable<Account> list(){
		return accountService.list();
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public Account create(@RequestBody Account account) throws NoSuchAlgorithmException, InvalidKeySpecException{
		return accountService.create(account);
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public Account read(@PathVariable(value="id") long id) throws AccountNotFoundException{
		Account account = accountService.read(id);
		if ( account == null ){
			throw new AccountNotFoundException("Account with id: " + id + " not found");
		}
		return account;
	}
	
	@RequestMapping( value="/{id}", method = RequestMethod.PUT)
	public Account update(@PathVariable(value="id") long id, @RequestBody Account account){
		return accountService.update(id, account);
	}
	
	@RequestMapping( value="/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable(value="id") int id){
		accountService.delete(id);
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
	public void handleAccountNotFound(AccountNotFoundException exception, HttpServletResponse response) throws IOException{
		response.sendError( HttpStatus.NOT_FOUND.value(), exception.getMessage());
	}

}
