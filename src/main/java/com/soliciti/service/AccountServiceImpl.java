package com.soliciti.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.soliciti.domain.Account;
import com.soliciti.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService{
	
	private AccountRepository accountRepository;
	
	@Autowired
	public AccountServiceImpl(AccountRepository accountRepository){
		this.accountRepository = accountRepository;
	}
	
	@Override
	public Iterable<Account> list(){
		return accountRepository.findAll();
	}
	
	@Override 
	public Account read(long id){
		return accountRepository.findOne(id);
	}
	
	@Override
	public Account create(Account account){
		return accountRepository.save(account);
	}
	
	@Override
	public void delete(long id){
		accountRepository.delete(id);
	}
	
	@Override
	public Account update(long id, Account update){
		Account account = accountRepository.findOne(id);
		if (update.getEmail() != null){
			account.setEmail(update.getEmail());
			account.setPassword(update.getPassword());
			account.setVerified(update.getVerified());
		}
		return accountRepository.save(account);
	}

}
