package com.soliciti.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import com.soliciti.domain.Account;

public interface AccountService {
	
	Iterable<Account> list();
	
	Account create(Account account) throws NoSuchAlgorithmException, InvalidKeySpecException;
	
	Account read(long id);
	
	Account update(long id, Account account); 
	
	void delete(long id);

}
